{*
* 2007-2019 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2020 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{* textarea after the condition block *}
<script type="text/javascript">
    $('#list_tables').on('change', function() {
        $('#list_tables_hide').val($(this).val());
        $('#OD_submit').attr('name', $('#OD_submit').attr('name') + 'AndStay').click();
    });

    $('document').ready(function() {
        $('#available_attributes').on('click', '.OD_add', function() {
            let to_add = $(this).val();
            if ($('.translatable-field').length > 0) {
                $('.translatable-field').each(function() {
                    if ($(this)[0].style.display != 'none') {
                        if ($(this)[0].querySelector('iframe').contentDocument.querySelector('#tinymce').innerText.length <= 1) {
                            $(this)[0].querySelector('iframe').contentDocument.querySelector('#tinymce').innerHTML = "%%" + to_add + "%%";
                        } else {
                            $(this)[0].querySelector('iframe').contentDocument.querySelector('#tinymce').innerText += "%%" + to_add + "%%";
                        }
                    }
                });
            } else {
                if ($('iframe')[0].contentDocument.querySelector('#tinymce').innerText.length <= 1) {
                    $('iframe')[0].contentDocument.querySelector('#tinymce').innerHTML = "%%" + to_add + "%%";
                } else {
                    $('iframe')[0].contentDocument.querySelector('#tinymce').innerText += "%%" + to_add + "%%";
                }

            }
        });
    });
</script>
