{*
* 2007-2019 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2020 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{$actual_content = 0}
{if isset($contents)}
<div class="box box-small clearfix">
{/if}
{foreach from=$contents item=content}

{if !array_key_exists($actual_content, $content)}
  {$actual_content = $actual_content + 1}
  </div>
  <div class="box box-small clearfix">
{/if}

{if array_key_exists($actual_content, $content)}
  {if $content[$actual_content]|substr:0:1 == '$'}
    <{($content[$actual_content]|escape:'html')|substr:1}>
  {elseif $content[$actual_content]|substr:0:1 == '/'}
    </{($content[$actual_content]|escape:'html')|substr:1}>
  {elseif $content[$actual_content]|substr:0:1 == ':'}
    {($content[$actual_content]|escape:'html')|substr:1}
  {/if}
{/if}

{/foreach}
{if isset($contents)}
</div>
{/if}
