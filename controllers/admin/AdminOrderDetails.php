<?php
/**
* 2007-2019 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2019 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class AdminOrderDetailsController extends ModuleAdminController
{
    public function __construct()
    {
        $this->bootstrap = true;
        $this->table = 'orderdetails_configuration';
        $this->className = 'OrderDetailsConfiguration';
        $this->tabClassName = 'AdminOrderDetails';
        $this->lang = true;
        $this->deleted = false;
        $this->colorOnBackground = false;
        $this->bulk_actions = array(
          'delete' => array('text' => 'Delete selected', 'confirm' => 'Delete selected items?'),
        );
        $this->context = Context::getContext();

        parent::__construct();

        $this->columns_name = array();

        $this->tables_name = array();
    }

    /**
     * Initialize the header Toolbar
     */
    public function initPageHeaderToolbar()
    {
        parent::initPageHeaderToolbar();

        if (empty($this->display)) {
            $this->page_header_toolbar_btn['desc-module-back'] = array(
              'href' => 'index.php?controller=AdminModules&token=' . Tools::getAdminTokenLite('AdminModules'),
              'desc' => 'Back',
              'icon' => 'process-icon-back',
            );
            $this->page_header_toolbar_btn['desc-module-new'] = array(
              'href' => 'index.php?controller=' . $this->tabClassName . '&add' . $this->table . '&token=' .
              Tools::getAdminTokenLite($this->tabClassName),
              'desc' => 'New',
              'icon' => 'process-icon-new',
            );
            $this->page_header_toolbar_btn['desc-module-reload'] = array(
              'href' => 'index.php?controller=' . $this->tabClassName . '&token=' .
              Tools::getAdminTokenLite($this->tabClassName),
              'desc' => 'Reload',
              'icon' => 'process-icon-refresh',
            );
        }
    }

    public function initContent()
    {
        parent::initContent();
    }

    public function getFieldsValue($obj)
    {
        $return = array();
        $default = array(
            'list_tables' => Tools::getIsset('list_tables_hide') ? Tools::getValue('list_tables_hide') : 'orders',
            'list_tables_hide' => ''
        );
        if (Tools::isSubmit('submitAdd' . $this->table)) {
            foreach (Language::getLanguages() as $lang) {
                $return['text'][$lang['id_lang']] = Tools::getValue('text_' . $lang['id_lang']);
            }
            $return['active'] = Tools::getValue('active');
            $return['title'] = Tools::getValue('title');
        } else {
            $return = parent::getFieldsValue($obj);
        }

        return array_merge($return, $default);
    }

    /**
     * Render the form (add and edit)
     */
    public function renderForm()
    {
        $this->fields_form = array(
            'legend' => array(
                'title' => $this->l('Configuration'),
                'icon' => 'icon-cogs',
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('Title'),
                    'name' => 'title',
                    'required' => true,
                    'desc' => $this->l('Only display in back-office.'),
                ),
                array(
                    'type' => 'switch',
                    'name' => 'active',
                    'label' => $this->l('Active: '),
                    'default_value' => true,
                    'values' => array(
                        array(
                            'id' => 'active',
                            'value' => true,
                            'label' => $this->l('Yes'),
                        ),
                        array(
                            'id' => 'inactive',
                            'value' => false,
                            'label' => $this->l('No'),
                        ),
                    ),
                ),
                'content' => array(
                    'type' => 'textarea',
                    'label' => $this->l('Text block you want to display : '),
                    'name' => 'text',
                    'id' => 'OD_textarea',
                    'cols' => 40,
                    'rows' => 10,
                    'class' => 'OD_textarea_class',
                    'autoload_rte' => true,
                    'lang' => true,
                    'required' => true,
                    'desc' =>
                        $this->l('Use the name of the attributes surrounded by 2 percentages : "%%attribute%%".'),
                    'hint' =>
                        $this->l('If you have several languages installed on your shop, don\'t forget to fill the') .
                        $this->l(' field in all the languages otherwise it will not be displayed for this language.'),
                ),
                array(
                    'type' => 'hidden',
                    'name' => 'list_tables_hide',
                ),
            ),
            'submit' => array(
                'title' => $this->l('Validate'),
                'id' => 'OD_submit',
            ),
        );

        $this->content .= parent::renderForm();

        $columns_name = array(
          'id_order',
          'id_customer'
        );

        $tables_name = array();

        $query = "SELECT SUBSTR(`table_name`, " . (Tools::strlen(_DB_PREFIX_) + 1) . ") AS 'table_name'
            FROM `information_schema`.`columns`
            WHERE `table_schema` = '" . _DB_NAME_ . "' AND `table_name` LIKE '" . _DB_PREFIX_ . "%'" .
            (!empty($columns_name) ?
            " AND (`column_name` = '" . implode("' OR `column_name` = '", $columns_name) . "')" :
            "") . (!empty($tables_name) ?
            " OR (`table_name` = '" . implode("' OR `table_name` = '", $tables_name) . "')" :
            "") . " GROUP BY `table_name`";

        $all_tables = Db::getInstance()->executeS(
            $query
        );

        $this->fields_form = array(
            'legend' => array(
                'title' => $this->l('Available tables'),
                'icon' => 'icon-cogs',
            ),
            'input' => array(
                array(
                    'type' => 'select',
                    'label' => $this->l('Select a table') . ':',
                    'name' => 'list_tables',
                    'required' => false,
                    'options' => array(
                        'query' => $all_tables,
                        'id' => 'table_name',
                        'name' => 'table_name',
                    ),
                ),
            ),
        );

        $this->content .= parent::renderForm();

        $attributes = array();

        $selected_table = Tools::getIsset('list_tables_hide') ? Tools::getValue('list_tables_hide') : 'orders';

        $attributes[$selected_table] = Db::getInstance()->executeS(
            "SHOW columns FROM `" . _DB_PREFIX_ . pSQL($selected_table) . "`"
        );

        $this->context->smarty->assign(array(
            'prefix' => _DB_PREFIX_,
            'attributes' => $attributes,
            'default_list' => $this->getDefaultList()
        ));

        $list_attributes = $this->context->smarty->fetch($this->module->getLocalPath() .
        'views/templates/admin/available_attributes.tpl');

        $test = $this->context->smarty->fetch($this->module->getLocalPath() .
        'views/templates/admin/configure.tpl');

        $this->content .= $list_attributes . $test;
    }

    /**
     * Render the configurations on admin panel
     */
    public function renderList()
    {
        $this->fields_list = array(
          'id_orderdetails_configuration' => array(
              'title' => $this->l('ID'),
              'align' => 'text-center',
              'class' => 'fixed-width-xs',
          ),
          'title' => array(
              'title' => $this->l('Name'),
          ),
          'active' => array(
              'title' => $this->l('Active'),
              'align' => 'text-center',
              'active' => 'status',
              'type' => 'bool',
          ),
          'text' => array(
              'title' => $this->l('Text'),
              'align' => 'text-center',
          ),
        );

        $this->addRowAction('edit');
        $this->addRowAction('delete');

        return parent::renderList();
    }

    /**
     * Check if the submit is valid.
     */
    public function initProcess()
    {
        if (Tools::isSubmit('submitAdd' . $this->table) && !Tools::isSubmit('submitAdd' . $this->table . 'AndStay')) {
            $text = "";
            foreach (Language::getLanguages() as $value) {
                if (Tools::getValue('text_' . $value['id_lang'])) {
                    $text = Tools::getValue('text_' . $value['id_lang']);
                    break;
                }
            }

            if (empty($text)) {
                $this->errors[] = $this->l('You must enter at least one text');
            }
        }
        return parent::initProcess();
    }

    /**
     * Save form data.
     */
    public function postProcess()
    {
        if (Tools::isSubmit('submitAdd' . $this->table) && !Tools::isSubmit('submitAdd' . $this->table . 'AndStay')) {
            return parent::postProcess();
        }
        return true;
    }

    public function getDefaultList()
    {
        return array(
            //ORDERS
            'orders-reference' => array(
                'description' => $this->l('Order Reference'),
                'title' => $this->l('Reference'),
                'type' => false,
            ),
            'orders-payment' => array(
                'description' => $this->l('Current state of the payment'),
                'title' => $this->l('Payment'),
                'type' => false,
            ),
            'orders-recyclable' => array(
                'description' => $this->l('Is this recyclable?'),
                'type' => 'boolean',
            ),
            'orders-gift' => array(
                'description' => $this->l('Is this a gift?'),
                'type' => 'boolean',
            ),
            'orders-giftmessage' => array(
                'description' => $this->l('Gift message'),
                'type' => false,
            ),
            'orders-shipping_number' => array(
                'description' => $this->l('Shipping number') . ' (' . $this->l('Some modules included') . ')',
                'title' => $this->l('Shipping number'),
                'type' => false,
            ),
            'orders-total_discounts' => array(
                'description' => $this->l('Total amount of discounts'),
                'type' => 'price',
            ),
            'orders-total_discounts_tax_incl' => array(
                'description' => $this->l('Total amount of discounts with taxes'),
                'type' => 'price',
            ),
            'orders-total_discounts_tax_excl' => array(
                'description' => $this->l('Total amount of discounts without taxes'),
                'type' => 'price',
            ),
            'orders-total_paid' => array(
                'description' => $this->l('Total amount paid'),
                'title' => $this->l('Total'),
                'type' => 'price',
            ),
            'orders-total_paid_tax_incl' => array(
                'description' => $this->l('Total amount paid with taxes'),
                'title' => $this->l('Total'),
                'type' => 'price',
            ),
            'orders-total_paid_tax_excl' => array(
                'description' => $this->l('Total amount paid without taxes'),
                'title' => $this->l('Total without taxes'),
                'type' => 'price',
            ),
            'orders-total_products' => array(
                'description' => $this->l('Total amount of products'),
                'type' => 'price',
            ),
            'orders-total_shipping' => array(
                'description' => $this->l('Total amount of shipping'),
                'type' => 'price',
            ),
            'orders-invoice_number' => array(
                'description' => $this->l('Invoice number'),
                'type' => false,
            ),
            'orders-delivery_number' => array(
                'description' => $this->l('Delivery number'),
                'type' => false,
            ),
            'orders-invoice_date' => array(
                'description' => $this->l('Invoice date'),
                'type' => 'date',
            ),
            'orders-delivery_date' => array(
                'description' => $this->l('Delivery date'),
                'type' => 'date',
            ),
            'orders-date_add' => array(
                'description' => $this->l('Date when the order was made'),
                'title' => $this->l('Date'),
                'type' => 'date',
            ),
            'orders-date_upd' => array(
                'description' => $this->l('Date when the order was updated'),
                'type' => 'date',
            ),

            //CUSTOMER
            'customer-company' => array(
                'description' => $this->l('Company name'),
                'type' => false,
            ),
            'customer-siret' => array(
                'description' => $this->l('APE number'),
                'type' => false,
            ),
            'customer-firstname' => array(
                'description' => $this->l('Firstname'),
                'title' => $this->l('Firstname'),
                'type' => false,
            ),
            'customer-lastname' => array(
                'description' => $this->l('Lastname'),
                'title' => $this->l('Lastname'),
                'type' => false,
            ),
            'customer-email' => array(
                'description' => $this->l('E-mail'),
                'title' => $this->l('E-mail'),
                'type' => false,
            ),
            'customer-birthday' => array(
                'description' => $this->l('Birthday'),
                'type' => 'date',
            ),
            'customer-newsletter' => array(
                'description' => $this->l('Is he subscribed to the newsletter?'),
                'type' => 'boolean',
            ),
            'customer-website' => array(
                'description' => $this->l('Website'),
                'type' => false,
            ),
            'customer-note' => array(
                'description' => $this->l('Note you write about him'),
                'type' => false,
            ),
            'customer-is_guest' => array(
                'description' => $this->l('Is he a guest?'),
                'type' => 'bool',
            ),
            'customer-date_add' => array(
                'description' => $this->l('Date of registration'),
                'type' => 'date',
            ),
            'customer-date_upd' => array(
                'description' => $this->l('Date of last connection'),
                'type' => 'date',
            ),

            //ADDRESS
            'address-alias' => array(
                'description' => $this->l('Alias'),
                'title' => $this->l('Alias'),
                'type' => false,
            ),
            'address-company' => array(
                'description' => $this->l('Company') . $this->l(' used for this address'),
                'title' => $this->l('Company'),
                'type' => false,
            ),
            'address-lastname' => array(
                'description' => $this->l('Lastname') . $this->l(' used for this address'),
                'title' => $this->l('Lastname'),
                'type' => false,
            ),
            'address-firstname' => array(
                'description' => $this->l('Firstname') . $this->l(' used for this address'),
                'title' => $this->l('Firstname'),
                'type' => false,
            ),
            'address-address1' => array(
                'description' => $this->l('Address'),
                'title' => $this->l('Address'),
                'type' => false,
            ),
            'address-address2' => array(
                'description' => $this->l('Address Complement'),
                'title' => $this->l('Complement'),
                'type' => false,
            ),
            'address-postcode' => array(
                'description' => $this->l('Postcode'),
                'title' => $this->l('Postcode'),
                'type' => false,
            ),
            'address-city' => array(
                'description' => $this->l('City'),
                'title' => $this->l('City'),
                'type' => false,
            ),
            'address-other' => array(
                'description' => $this->l('Other'),
                'type' => false,
            ),
            'address-phone' => array(
                'description' => $this->l('Phone number') . $this->l(' used for this address'),
                'title' => $this->l('Phone'),
                'type' => false,
            ),
            'address-vat_number' => array(
                'description' => $this->l('VAT identification number'),
                'type' => false,
            ),

            //Country
            'country-iso_code' => array(
                'description' => $this->l('ISO code'),
                'type' => false,
            ),
            'country-call_prefix' => array(
                'description' => $this->l('Prefix to phone number'),
                'type' => false,
            ),

            //COUNTRY_LANG
            'country_lang-name' => array(
                'description' => $this->l('Country name'),
                'title' => $this->l('Country'),
                'type' => false,
            ),

            //ORDER_STATE
            'order_state-shipped' => array(
                'description' => $this->l('Is shipped?'),
                'type' => 'bool',
            ),
            'order_state-paid' => array(
                'description' => $this->l('Is paid?'),
                'type' => 'bool',
            ),

            //ORDER_STATE_LANG
            'order_state_lang-name' => array(
                'description' => $this->l('Order State'),
                'title' => $this->l('Status'),
                'type' => 'order-state',
            ),

            //CART
            'cart-recyclable' => array(
                'description' => $this->l('Is this recyclable?'),
                'type' => 'boolean',
            ),
            'cart-gift' => array(
                'description' => $this->l('Is this a gift?'),
                'type' => 'boolean',
            ),
            'cart-giftmessage' => array(
                'description' => $this->l('Gift message'),
                'type' => false,
            ),
            'cart-date_add' => array(
                'description' => $this->l('Date when the cart was created'),
                'type' => 'date',
            ),
            'cart-date_upd' => array(
                'description' => $this->l('Date when the cart was updated'),
                'title' => $this->l('Date'),
                'type' => 'date',
            ),
            'cart-total' => array(
                'description' => $this->l('Total amount of the cart'),
                'title' => $this->l('Total'),
                'type' => false,
            ),
            'cart-status' => array(
                'description' => $this->l('Order ID'),
                'title' => $this->l('Order ID'),
                'type' => false,
            ),

            //CARRIER
            'carrier-name' => array(
                'description' => $this->l('Carrier name'),
                'title' => $this->l('Carrier'),
                'type' => false,
            ),
            'carrier-url' => array(
                'description' => 'URL',
                'type' => false,
            ),

            //CUSTOMER_THREAD
            'customer_thread-status' => array(
                'description' => $this->l('Status'),
                'title' => $this->l('Status'),
                'type' => 'thread-status',
            ),
            'customer_thread-email' => array(
                'description' => $this->l('E-mail'),
                'title' => $this->l('E-mail'),
                'type' => false,
            ),
            'customer_thread-date_add' => array(
                'description' => $this->l('Date when the thread was created'),
                'type' => 'date',
            ),
            'customer_thread-date_upd' => array(
                'description' => $this->l('Date when the thread was updated'),
                'title' => $this->l('Last message'),
                'type' => 'date',
            ),

            //CUSTOMER_MESSAGE
            'customer_message-message' => array(
                'description' => $this->l('Message'),
                'title' => $this->l('Message'),
                'type' => false,
            ),

            //CONTACT_LANG
            'contact_lang-name' => array(
                'description' => $this->l('Contact name (Webmaster, ...)'),
                'title' => $this->l('Contact'),
                'type' => false,
            ),

            //PRODUCT
            'product-ean13' => array(
                'description' => 'EAN-13' . $this->l(' of all the products of the order / cart'),
                'type' => false,
            ),
            'product-isbn' => array(
                'description' => 'ISBN' . $this->l(' of all the products of the order / cart'),
                'type' => false,
            ),
            'product-upc' => array(
                'description' => 'UPC' . $this->l(' of all the products of the order / cart'),
                'type' => false,
            ),
            'product-ecotax' => array(
                'description' => $this->l('Ecotax') . $this->l(' of all the products of the order / cart'),
                'type' => false,
            ),
            'product-minimal_quantity' => array(
                'description' => $this->l('Minimal quantity') . $this->l(' of all the products of the order / cart'),
                'type' => false,
            ),
            'product-low_stock_alert' => array(
                'description' => $this->l('Low stock alert') . $this->l(' of all the products of the order / cart'),
                'type' => false,
            ),
            'product-price' => array(
                'description' => $this->l('Price') . $this->l(' of all the products of the order / cart'),
                'type' => 'price',
            ),
            'product-wholesale_price' => array(
                'description' => $this->l('Wholesale price') . $this->l(' of all the products of the order / cart'),
                'type' => 'price',
            ),
            'product-additional_shipping_cost' => array(
                'description' => $this->l('Additional shipping cost') .
                $this->l(' of all the products of the order / cart'),
                'type' => 'price',
            ),
            'product-reference' => array(
                'description' => $this->l('Reference') . $this->l(' of all the products of the order / cart'),
                'title' => $this->l('Products'),
                'type' => false,
            ),
            'product-supplier_reference' => array(
                'description' => $this->l('Supplier reference') . $this->l(' of all the products of the order / cart'),
                'type' => false,
            ),
            'product-location' => array(
                'description' => $this->l('Location') . $this->l(' of all the products of the order / cart'),
                'type' => false,
            ),
            'product-width' => array(
                'description' => $this->l('Width') . $this->l(' of all the products of the order / cart'),
                'type' => false,
            ),
            'product-height' => array(
                'description' => $this->l('Height') . $this->l(' of all the products of the order / cart'),
                'type' => false,
            ),
            'product-depth' => array(
                'description' => $this->l('Depth') . $this->l(' of all the products of the order / cart'),
                'type' => false,
            ),
            'product-weight' => array(
                'description' => $this->l('Weight') . $this->l(' of all the products of the order / cart'),
                'type' => false,
            ),

            //CUSTOMIZED_DATA
            'customized_data-value' => array(
                'description' => $this->l('Customized texts'),
                'title' => $this->l('Customized texts'),
                'type' => false,
            ),

            //LANG
            'lang-name' => array(
                'description' => $this->l('Language used'),
                'title' => $this->l('Language'),
                'type' => false,
            ),
        );
    }
}
