<?php
/**
* 2007-2019 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2019 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

include_once _PS_MODULE_DIR_ . 'orderdetails/classes/OrderDetailsConfiguration.php';

class Orderdetails extends Module
{
    protected $config_form = false;

    public function __construct()
    {
        $this->name = 'orderdetails';
        $this->tab = 'administration';
        $this->version = '1.1.0';
        $this->author = 'Ciren';
        $this->need_instance = 1;
        $this->module_key = '14c4091cd57a845671de3fb3bbd36669';

        /*
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Add informations about order details');
        $this->description = $this->l('With the possibility of using datas from the database');

        $this->confirmUninstall = $this->l('Are you sure you want to uninstall this module ?');

        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        include dirname(__FILE__) . '/sql/install.php';

        return parent::install() &&
            $this->addTab() &&
            $this->registerHook('displayOrderDetail');
    }

    private function addTab()
    {
        $subtab = new Tab();
        $subtab->class_name = 'AdminOrderDetails';
        if (version_compare(_PS_VERSION_, '1.7', '<')) {
            $subtab->id_parent = Tab::getIdFromClassName('AdminAdmin');
        } else {
            $subtab->id_parent = Tab::getIdFromClassName('AdminParentThemes');
        }
        $subtab->module = $this->name;
        $subtab->name[(int) Configuration::get('PS_LANG_DEFAULT')] = $this->displayName;
        $subtab->add();

        return true;
    }

    /**
     * Redirect to the the list
     */
    public function getContent()
    {
        $link = new Link();
        $link = $link->getAdminLink('AdminOrderDetails', true);

        return Tools::redirectAdmin($link);
    }

    public function hookDisplayOrderDetail()
    {
        // Define all global variables we could need
        $id_customer = (int)$this->context->customer->id;
        $id_customer = (int)$id_customer;
        $id_lang = (int)$this->context->language->id;
        // $id_groups[] = Db::getInstance()->executeS(
        // 'SELECT id_group
        // FROM '._DB_PREFIX_.'customer_group WHERE id_customer='.$id_customer);
        $id_order = (int)Tools::getValue('id_order');
        $id_order = (int)$id_order;

        $configurations = Db::getInstance()->executeS(
            "SELECT `a`.`id_orderdetails_configuration`, `text`
            FROM `" . _DB_PREFIX_ . "orderdetails_configuration` `a`
            INNER JOIN `" . _DB_PREFIX_ . "orderdetails_configuration_lang` `b`
            ON `a`.`id_orderdetails_configuration` = `b`.`id_orderdetails_configuration`
            WHERE `id_lang` = '" . (int)$id_lang . "' AND `active` = '1' AND `text` <> ''"
        );

        $contents = array();
        $content_num = 0;

        $columns_name = array(
            'id_order',
            'id_customer'
        );

        foreach ($configurations as $config) {
            if (empty($config['text'])) {
                continue;
            }

            if (preg_match_all("/%%(\w+)-(\w+)%%/", $config['text'], $vars)) {
                $text_array = preg_split('/%%\w+-\w+%%/', $config['text']);

                // tables
                foreach ($vars[1] as $key => $table) {
                    $vars[0][$key] = "";
                    $table = _DB_PREFIX_ . $table;

                    $query = "SELECT `column_name`
                        FROM `information_schema`.`columns`
                        WHERE `table_schema` = '" . _DB_NAME_ . "'
                        AND (`column_name` = '" . implode("' OR `column_name` = '", $columns_name) . "')
                        AND (`table_name` = '" . pSQL($table) . "')
                        GROUP BY `column_name`";

                    $exists = Db::getInstance()->executeS($query);

                    if (!empty($exists)) {
                        foreach ($exists as $column) {
                            $column = $column['column_name'];
                            if (in_array($column, $columns_name)) {
                                $vars[0][$key] = Db::getInstance()->getValue(
                                    "SELECT GROUP_CONCAT(DISTINCT `" . pSQL($vars[2][$key]) . "` SEPARATOR ', ')
                                    FROM `" . pSQL($table) . "`
                                    WHERE `" . pSQL($column) . "` = '" . (int)${$column} . "'"
                                );
                                $vars[0][$key] = empty($vars[0][$key]) ? "" : $vars[0][$key];
                                break;
                            }
                        }
                    }
                }
                $text = "";

                if (!empty($text_array)) {
                    foreach ($text_array as $text_key => $text_value) {
                        $text .= $text_value;
                        if (isset($vars[0][$text_key])) {
                            $text .= $vars[0][$text_key];
                        }
                    }
                }

                while (!empty($text) && is_string($text)) {
                    if (preg_match("/^<\/([^>]*)>(.*)/s", $text, $buffer_content, PREG_OFFSET_CAPTURE)) {
                        $text = $buffer_content[2][0];
                        $contents[] = array($content_num => '/' . $buffer_content[1][0]);
                    } elseif (preg_match('/^<([^>]*)>(.*)/s', $text, $buffer_content, PREG_OFFSET_CAPTURE)) {
                        $text = $buffer_content[2][0];
                        $contents[] = array($content_num => '$' . $buffer_content[1][0]);
                    } elseif (preg_match('/^([^<]*)(<.*)/s', $text, $buffer_content, PREG_OFFSET_CAPTURE)) {
                        $text = $buffer_content[2][0];
                        $contents[] = array($content_num => ':' . $buffer_content[1][0]);
                    } elseif (preg_match("/^\n(.*)/s", $text, $buffer_content, PREG_OFFSET_CAPTURE)) {
                        $text = $buffer_content[2][0];
                        $contents[] = array($content_num => '$br');
                    } elseif (preg_match("/^\s*(.*)/s", $text, $buffer_content, PREG_OFFSET_CAPTURE)) {
                        $text = $buffer_content[1][0];
                        $contents[] = array($content_num => ': ');
                    } else {
                        break;
                    }
                }
            }
        }
        print_r($contents);
        if (!empty($contents)) {
            // Assign $contents to template
            $this->context->smarty->assign(array(
                'contents' => $contents,
            ));

            // Display template.
            return $this->display(__FILE__, 'displayOrderDetails.tpl');
        }
    }
}
